# Front новостного портала

Next.js сервис, запускается на 80 порту, поэтому предварительно на машине нужно оставить web серверы, если таковые имеются.  

Обслуживает сразу все домены которые в A записи указывают на текущий сервер, что удобно для поднятия сразу пачки сайтов.

## Установка
`git clone https://krylov123@bitbucket.org/krylov123/news-next-front.git`  

`cd news-next-front`  

`npm install`  

## Запуск в dev
Просто запускаем  
`npm run dev`  

## Запуск production (Linux)
Отредактировать bash скрипт `forever.sh` заменив строчку `cd /root/news-next-front/` на актуальный путь к репозиторию на своем сервере.

Для проверки работы скрипта запустить:
`bash forever.sh`  

### Автозапуск (Linux)
Если сервис стал доступен на http://localhost:80 то можно добавить `forever.sh` в cron или другой планировщик заданий.  
`* *    * * *   root    bash /root/news-next-front/forever.sh`  
Не забыв заменить путь на актуальный.

## Сборка
Ассеты и js автоматически компилируются в dev режиме:
`npm run dev`

При необходимости скомпилить вручную и запустить сервис:
`npm run build`  
`npm run start`  

##TODO
* привязать ssl сертификаты (возможно ли при мультидоменности)
* ~~FIX: не отрабатывает onClick event в пагинаторе~~
* добавить SEO оптимизацию (meta, microdata)
* ~~добавить nav панель~~
* добавить авторизацию (получение jwt токена по логин/паролю)
* добавить режим редактирования новости после авторизации
* больше кастомизации под домен (заголовки, цвет меню и тд), вынести в конфиг
* добавить обработчики ошибок в async запросы
* добавить авторазметку (разбиение, выделение) для длинных текстов
