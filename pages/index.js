import React from 'react';
import fetch from 'isomorphic-unfetch';
import Link from 'next/link';
import Paginator from "../components/Paginator.js"
import config from "../news.config.js";
import NewsListElement from "../components/NewsListElement";

class HomePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            news: props.news,
            currentPage: 1,
            projectName: props.projectName
        };
    }

    static async getInitialProps(context) {
        const host = context.req.headers.host;
        let projectName = config.defaultProject;
        if (config.domainMap.hasOwnProperty(host)) projectName = config.domainMap[host];

        const result = await this.getNews(projectName);

        return {...result, projectName: projectName}
    };

    static async getNews(projectName, page = 1, cb) {
        const res = await fetch('https://api.coronavir.info/api/v1/news?project=' + projectName + '&page=' + page);
        const json = await res.json();
        if (typeof cb !== 'undefined') cb(json);
        return {news: json}
    };

    setPageHandler = (index) => {
        let context = this;
        HomePage.getNews(this.state.projectName, index, (result) => {
            context.setState({
                news: result,
                currentPage: index
            });
        });
    };

    render() {
        let context = this;
        const news = context.state.news;

        const htmlNews = news.data.map((element) => {
            return (
                <div className="col-md-10" key={element.data.hash}>
                    <NewsListElement news={element.data}/>
                </div>
            );
        });

        return (
            <React.Fragment>
                <div className={"row"}>
                    {htmlNews}
                </div>
                <div className={"row"}>
                    <div className={"offset-md-4 col-md4"}>
                        <Paginator totalPages={(news.meta.total / news.meta.per_page + 1)}
                                   setPageHandler={context.setPageHandler}
                                   curPage={context.state.currentPage}
                        />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}


HomePage.propTypes = {};

export default HomePage;