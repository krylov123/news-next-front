import App from 'next/app';
import {withRouter} from 'next/router';
import "../resources/sass/style.scss";
import MainLayout from '../components/Layout.js';
import config from '../news.config.js';

function MyApp({Component, pageProps, projectName}) {
    return (
        <MainLayout projectName={projectName}>
            <Component {...pageProps} />
        </MainLayout>
    );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
MyApp.getInitialProps = async (appContext) => {
    const host = appContext.ctx.req.headers.host;
    let projectName = config.defaultProject;
    if (config.domainMap.hasOwnProperty(host)) projectName = config.domainMap[host];
    // calls page's `getInitialProps` and fills `appProps.pageProps`
    const appProps = await App.getInitialProps(appContext);

    return {...appProps, projectName: projectName}
};

export default withRouter(MyApp)