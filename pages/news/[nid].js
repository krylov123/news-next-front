import React from 'react';
import fetch from 'isomorphic-unfetch';
import Link from 'next/link';
import PropTypes from 'prop-types';

class NewsIdPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    static async getInitialProps(context) {
        const res = await fetch('https://api.coronavir.info/api/v1/news/' + parseInt(context.query.nid));
        const json = await res.json();
        return {news: json}
    };

    render() {
        const news = this.props.news.data;
        return (
            <div className={"row"}>
                <div className="col-md-12 blog-main">
                    <div className="blog-post">
                        <h2 className={"blog-post-title"}>{news.title}</h2>
                        <p className={"blog-post-media"}>
                            <img
                                src={(news.image_link === "") ? null : news.image_link}
                                alt={news.title}
                                className={"news-img-thumbnail"}
                            />
                        </p>
                        <p className="blog-post-meta">{news.pub_date.split(" ")[0]}</p>
                        <p>{news.preview}</p>
                        <hr/>
                        <p>{news.description}</p>
                        <p><a href={news.link}>Читать оригинал...</a></p>
                        <Link href="/"><a>Назад</a></Link>
                    </div>
                </div>
            </div>
        );
    }
}


NewsIdPage.propTypes = {};

export default NewsIdPage;