const Navbar = ({title}) => {
    return (
        <nav className="navbar navbar-dark bg-dark justify-content-between">
            <a className="navbar-brand">{title}</a>
            <form className="form-inline">
                <span className={"navbar"} style={{color: 'whitesmoke'}}>Авторизация</span>
                <input className="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Search"/>
                <button className="btn btn-warning my-2 my-sm-0" type="submit">Искать</button>
            </form>
        </nav>
    );
};

export default Navbar;