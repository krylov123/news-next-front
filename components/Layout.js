import Navbar from "./Navbar.js";

const MainLayout = ({children, projectName}) => {
    return (
        <div width={"100%"} height={"100%"} className={projectName}>
            <Navbar title={"Новости"} />
            <div className={"container"} style={{padding: '10px'}}>
                <div className="content-wrapper">{children}</div>
            </div>
        </div>
    );
}

export default MainLayout;