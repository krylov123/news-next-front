import React from 'react';

class PaginatorButton extends React.Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        pageIndex: 1,
        buttonText: "",
        buttonClass: "",
        clickHandler: () => { console.warn("PaginatorButton: click handler is not set"); }
    };

    render() {
        const props = this.props;
        return (
            <li className={props.buttonClass}>
                <a className={"page-link"} onClick={props.clickHandler.bind(null, props)}>{props.buttonText}</a>
            </li>
        );
    }
}

export default PaginatorButton;
