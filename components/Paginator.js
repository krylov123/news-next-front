import React from 'react';
import PaginatorButton from './PaginatorButton.js';

class Paginator extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    static defaultProps = {
        curPage: 1,
        totalPages: 1,
        maxCount: 10,
        showPrevNextButton: true,
        showFirstLastButton: true,
        ulClassName: "pagination pull-left",
        setPageHandler: () => {
            console.warn("Paginator: setPageHandler is not set");
        }
    };

    /* How many page buttons must be shown */
    windowCount = () => {
        return (this.props.totalPages > this.props.maxCount) ? this.props.maxCount : this.props.totalPages;
    };

    /* Calculate half of window by modulo */
    halfWindowCount = () => {
        const windowCount = this.windowCount();
        return (windowCount - windowCount % 2) / 2;
    };

    window = () => {
        const curPage = this.props.curPage;
        const halfWindowCount = this.halfWindowCount();

        //Decide which half is lesser
        let leftSideWindow = halfWindowCount;
        if ((curPage - leftSideWindow) < 0) leftSideWindow = curPage - 1;

        let rightSideWindow = curPage + halfWindowCount;
        if ((curPage + rightSideWindow) > this.props.totalPages) rightSideWindow = this.props.totalPages - curPage;

        //If right side lesser than left
        let finishPage = curPage + rightSideWindow;
        let startPage = finishPage - (this.props.maxCount - 1);
        if (startPage < 1) startPage = 1;

        //If left side lesser than right
        if (leftSideWindow <= rightSideWindow) {
            startPage = curPage - leftSideWindow;
            if (startPage < 1) startPage = 1;
            finishPage = startPage + this.props.maxCount - 1;
        }

        //final recheck
        if (startPage < 1) startPage = 1;
        if (finishPage > this.props.totalPages) finishPage = this.props.totalPages;

        return {
            startPage: startPage,
            finishPage: finishPage,
            leftSideWindow: curPage - startPage,
            rightSideWindow: finishPage - curPage
        };

    };

    generatePages = (pageStart, pageFinish) => {
        let buttonIndecies = [];

        for (var i = pageStart; i <= pageFinish; i++) {
            buttonIndecies.push(i);
        }

        return buttonIndecies.map(index =>
            (
                <li key={'buttonInd' + index}
                    className={(this.props.curPage === index) ? "page-item active" : "page-item"}>
                    <a className={"page-link"} onClick={this.props.setPageHandler.bind(null, index)}>{index}</a>
                </li>
            )
        );
    };

    generatePaginator = () => {
        const pagesWindow = this.window();

        let buttons = this.generatePages(pagesWindow.startPage, pagesWindow.finishPage);

        if ((pagesWindow.leftSideWindow > 2)) {
            if (this.props.curPage > (pagesWindow.leftSideWindow + 1)) {
                const firstButtonClickHandler = this.props.setPageHandler.bind(null, 1);
                buttons[0] = (
                    <PaginatorButton pageIndex={1}
                                     buttonText='1'
                                     buttonClass={(this.props.curPage === 1) ? "page-item active" : "page-item"}
                                     clickHandler={firstButtonClickHandler}
                    />
                );
                buttons[1] = (
                    <PaginatorButton pageIndex={1}
                                     buttonText='...'
                                     buttonClass={"page-item disabled"}
                    />
                );
            }
        }

        if (pagesWindow.rightSideWindow > 2) {
            if ((this.props.curPage < (this.props.totalPages - pagesWindow.rightSideWindow))) {
                const lastButtonClickHandler = this.props.setPageHandler.bind(null, this.props.totalPages);
                buttons[buttons.length - 2] = (
                    <PaginatorButton pageIndex={this.props.totalPages} buttonText='...'
                                     buttonClass={"page-item disabled"}
                    />
                );
                buttons[buttons.length - 1] = (
                    <PaginatorButton pageIndex={this.props.totalPages}
                                     buttonText={this.props.totalPages}
                                     buttonClass={(this.props.curPage === this.props.totalPages) ? "page-item active" : "page-item"}
                                     clickHandler={lastButtonClickHandler}
                    />
                );
            }
        }

        return buttons;
    };

    generatePrevButton = () => {
        if (!this.props.showPrevNextButton) return null;

        let buttonClass = "";
        let nextIndex = this.props.curPage - 1;
        let clickHandler = this.props.setPageHandler.bind(null, nextIndex);
        if (nextIndex < 1) {
            buttonClass = 'page-item disabled';
            clickHandler = () => {
                return;
            };
        }
        return (
            <PaginatorButton
                pageIndex={nextIndex}
                buttonText='&laquo;'
                buttonClass={buttonClass}
                clickHandler={clickHandler}
            />
        );
    };

    generateNextButton = () => {
        if (!this.props.showPrevNextButton) return null;

        let buttonClass = "";
        let nextIndex = this.props.curPage + 1;
        let clickHandler = this.props.setPageHandler.bind(null, nextIndex);
        if (nextIndex > this.props.totalPages) {
            buttonClass = 'page-item disabled';
            clickHandler = () => {
                return;
            };
        }
        return (
            <PaginatorButton
                pageIndex={nextIndex}
                buttonText='&raquo;'
                buttonClass={buttonClass}
                clickHandler={clickHandler}
            />
        );
    };

    render() {

        let prevButton = this.generatePrevButton();
        let nextButton = this.generateNextButton();
        let buttons = this.generatePaginator();

        return (
            <ul className={this.props.ulClassName}>
                {prevButton}
                {buttons}
                {nextButton}
            </ul>
        );
    }
}

export default Paginator;
