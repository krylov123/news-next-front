import React from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';

const noImage = 'https://upload.wikimedia.org/wikipedia/en/6/60/No_Picture.jpg';

class NewsListElement extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            waiting: false,
            collapse: true,
            editMode: false,
            originalNews: Object.assign({}, props.news),
            currentNews: Object.assign({}, props.news)
        };
    }

    static shortPreview = (str, maxLen = 500, separator = ' ') => {
        if (str.length <= maxLen) return str;
        return str.substr(0, str.lastIndexOf(separator, maxLen));
    };

    toggleCollapse = () => {
        this.setState({
            collapse: !this.state.collapse
        });
    };

    toggleEditMode = () => {
        this.setState({
            editMode: !this.state.editMode
        });
    };

    static defaultProps = {
        isEditAllowed: false
    };

    render() {
        let context = this;
        const news = this.props.news;
        const nid = news.id;

        let text = "";
        let nextLink = null;
        if (this.state.collapse) {
            text = NewsListElement.shortPreview(news.preview);
        } else {
            text = news.description;
            nextLink = (<a href={news.link} rel="nofollow">Читать оригинал...</a>)
            /*nextLink = (
                <Link href="/news/[nid]" as={`/news/${nid}`}>
                    <a className="stretched-link">Читать далее... </a>
                </Link>
            );*/
        }

        const editModeClass = (context.state.editMode) ? "" : "hidden";

        return (
            <div
                onClick={this.toggleCollapse}
                className={"row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative news"}
            >
                <div className="col p-4 d-flex flex-column position-static">
                    <strong className="d-inline-block mb-2 text-primary">{news.category}</strong>
                    <h3 className="mb-0">{news.title}</h3>
                    <input className={'form-control input ' + editModeClass} value={context.state.currentNews.title}/>
                    <div className="mb-1 text-muted">{news.pub_date.split(" ")[0]}</div>
                    <p className="card-text mb-auto">{text}</p>
                    {nextLink}
                </div>
                <div className="col-auto d-none d-lg-block">
                    <img
                        src={(news.image_link === "") ? noImage : news.image_link}
                        alt={news.title}
                        width="200" height="100%"
                        className={"news-img-thumbnail"}
                    />
                </div>
            </div>
        );
    }
}

NewsListElement.propTypes = {news: PropTypes.object, isEditAllowed: PropTypes.bool};

export default NewsListElement;