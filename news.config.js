const config = {
    defaultProject: 'coronavirus',
    domainMap: {
        'localhost:8081': 'coronavirus',
        'newsfactory.loc:8081': 'coronavirus',
        'coronavir.info': 'coronavirus',
        'elonmusknews.world': 'elonmusk',
        'oscar-news.com': 'oscar'
    }
};

export default config;