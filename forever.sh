#!/bin/bash

OUTPUT="$(ps aux | grep -v "grep" | grep "next start -p 80" | wc -l)"
echo "${OUTPUT}"

if [ "${OUTPUT}" == "0" ];
then
        cd /root/news-next-front/
        git pull origin master
        npm run build
        npm run start
else
        echo "Process already run"
fi

exit 0
